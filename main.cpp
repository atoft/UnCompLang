#include <iostream>
#include <vector>
#include <cassert>
#include <fstream>
#include <cstring>

#include "Globals.h"
#include "Lexer.h"

void WriteELFHeaders(std::vector<u8>& OutBinary)
{
	// https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
	// https://github.com/cj1128/tiny-x64-helloworld

	// File header
	OutBinary.reserve(64);

	// Magic number
	OutBinary.push_back(0x7f);
	OutBinary.push_back(0x45);
	OutBinary.push_back(0x4c);
	OutBinary.push_back(0x46);

	// 64-bit
	OutBinary.push_back(0x02);

	// Little endian
	OutBinary.push_back(0x01);

	// ELF version
	OutBinary.push_back(0x01);

	// ABI = Linux
	OutBinary.push_back(0x03);

	// Padding
	OutBinary.insert(OutBinary.end(), 8, 0x00);

	// Object file type = Binary
	OutBinary.push_back(0x02);
	OutBinary.push_back(0x00);

	// ISA = x86-64
	OutBinary.push_back(0x3e);
	OutBinary.push_back(0x00);

	// version = 1
	OutBinary.push_back(0x01);
	OutBinary.insert(OutBinary.end(), 3, 0x00);

	// Entry point
	// Calculated as offset to the start of the program, plus the offset of the 64-bit virtual address space in Linux (0x400000)
	OutBinary.push_back(0x78);
	OutBinary.push_back(0x00);
	OutBinary.push_back(0x40);
	OutBinary.insert(OutBinary.end(), 5, 0x00);

	// Program header offset
	OutBinary.push_back(0x40);
	OutBinary.insert(OutBinary.end(), 7, 0x00);

	// Section header offset
	OutBinary.insert(OutBinary.end(), 8, 0x00);

	// e_flags
	OutBinary.insert(OutBinary.end(), 4, 0x00);

	// e_ehsize = 64
	OutBinary.push_back(0x40);
	OutBinary.push_back(0x00);

	// e_phentsize
	OutBinary.push_back(0x38);
	OutBinary.push_back(0x00);

	// e_phnum
	OutBinary.push_back(0x01);
	OutBinary.push_back(0x00);

	// e_shentsize
	OutBinary.insert(OutBinary.end(), 2, 0x00);

	// e_shnum
	OutBinary.insert(OutBinary.end(), 2, 0x00);

	// e_shstrndx
	OutBinary.insert(OutBinary.end(), 2, 0x00);

	assert(OutBinary.size() == 0x40);

	// Program header
	OutBinary.reserve(OutBinary.size() + 0x38);

	// Segment type = Loadable segment
	OutBinary.push_back(0x01);
	OutBinary.insert(OutBinary.end(), 3, 0x00);

	// Flags = Readable & Executable
	OutBinary.push_back(0x05);
	OutBinary.insert(OutBinary.end(), 3, 0x00);

	// Offset
	OutBinary.insert(OutBinary.end(), 8, 0x00);

	// Virtual address
	OutBinary.push_back(0x00);
	OutBinary.push_back(0x00);
	OutBinary.push_back(0x40);
	OutBinary.insert(OutBinary.end(), 5, 0x00);

	// Physical address
	OutBinary.insert(OutBinary.end(), 8, 0x00);

	// p_filesz Size of the segment in the file image
	OutBinary.insert(OutBinary.end(), 8, 0x00);

	// p_memsz Size of the segment in memory
	OutBinary.insert(OutBinary.end(), 8, 0x00);

	// p_align
	OutBinary.push_back(0x00);
	OutBinary.push_back(0x10);
	OutBinary.insert(OutBinary.end(), 6, 0x00);

	assert(OutBinary.size() == 0x40 + 0x38);
}

void WriteELFSizes(std::vector<u8>& OutBinary)
{
	u64* FilesizeZ = reinterpret_cast<u64*>(&OutBinary[0x40 + 0x20]);
	u64* MemsizeZ = reinterpret_cast<u64*>(&OutBinary[0x40 + 0x28]);

	const u64 ProgramSize = OutBinary.size() - (0x40 + 0x38);

	*FilesizeZ = ProgramSize;
	*MemsizeZ = ProgramSize;
}

int main()
{
	std::cout << "UnCompLang" << std::endl;

	const std::string SourceCode = "(\"Hello world!\");";

	Lexer MyLexer;
	const auto Result = MyLexer.Lex(SourceCode);

	std::vector<u8> Binary;
	WriteELFHeaders(Binary);

	// https://www.intel.com/content/dam/develop/external/us/en/documents/introduction-to-x64-assembly-181178.pdf
	// https://disasm.pro/

	// Print to stdout

	// mov rax, 1 (syscall number for write)
	Binary.insert(Binary.end(), { 0x48, 0xC7, 0xC0, 0x01, 0x00, 0x00, 0x00 }   );

	// mov rdi, 1 (stdout)
	Binary.insert(Binary.end(), { 0x48, 0xC7, 0xC7, 0x01, 0x00, 0x00, 0x00 }    );

	// mov rsi, 0 (to be filled with memory address of Message)
	// TODO Understand exactly how the second byte of the opcode is constructed to give a 64-bit literal.
	const u64 IndexForMessageAddress = Binary.size() + 2;
	Binary.insert(Binary.end(), { 0x48, 0xBE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }    );

	// mov rdx, 0 (to be filled with message length)
	const u64 IndexForMessageLength = Binary.size() + 3;
	Binary.insert(Binary.end(), { 0x48, 0xC7, 0xC2, 0x00, 0x00, 0x00, 0x00 }    );

	// syscall
	Binary.insert(Binary.end(), { 0x0F, 0x05 } );

	// Exit with code 42

	// mov rax, 60 (syscall number for exit)
	Binary.insert(Binary.end(), { 0x48, 0xC7, 0xC0, 0x3C, 0x00, 0x00, 0x00 }   );

	// mov rdi, 42
	Binary.insert(Binary.end(), { 0x48, 0xC7, 0xC7, 0x2A, 0x00, 0x00, 0x00 }    );

	// syscall
	Binary.insert(Binary.end(), { 0x0F, 0x05 } );


	// String data
	const std::string Message = "Hello World!\n";
	const u64 IndexForMessageData = Binary.size();

	// Write the string into the binary.
	Binary.resize(Binary.size() + Message.size() + 1);
	void* StringStart = reinterpret_cast<u64*>(&Binary[IndexForMessageData]);
	std::memcpy(StringStart, Message.data(), Message.size() + 1);

	// Backfill the address of the string into the relevant mov instruction.
	u32* MessageLengthAddress = reinterpret_cast<u32*>(&Binary[IndexForMessageLength]);
	*MessageLengthAddress = static_cast<u32>(Message.size() + 1);

	// Backfill the length of the string into the relevant mov instruction.
	const u64 MessageMemoryAddress = 0x400000 + IndexForMessageData;
	u64* StringAddress = reinterpret_cast<u64*>(&Binary[IndexForMessageAddress]);
	*StringAddress = MessageMemoryAddress;

	// Backfill the length of the program into the ELF header.
	WriteELFSizes(Binary);

	std::fstream outfile;
	outfile.open("helloworld", std::fstream::out);

	outfile.write(reinterpret_cast<char*>(Binary.data()), Binary.size());

	outfile.flush();
	outfile.close();

	return 0;
}
