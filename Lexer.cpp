
#include "Lexer.h"

auto Lexer::Lex(const std::string& SourceCode) -> Result<std::vector<Token>>
{
	std::vector<Token> Tokens;

	for (auto It = SourceCode.begin(); It != SourceCode.end(); ++It)
	{
		const std::optional<TokenType> MaybeToken = LexCharToTokenType(*It);

		if (MaybeToken)
		{
			Tokens.push_back(Token::MakeFromSource(*MaybeToken, It, It + 1));
			continue;
		}

		switch (*It)
		{
		case '"':
		{
			auto EndIt = It + 1;

			while (*EndIt != '"')
			{
				// TODO Support escaped quotes.

				++EndIt;

				if (EndIt == SourceCode.end())
				{
					return Error<std::vector<Token>>("Unexpected end of file found during string literal (missing '\"').");
				}
			}

			Tokens.push_back(Token::MakeFromSource(TokenType::String, It + 1, EndIt));
			It = EndIt;
			break;
		}
		case ' ':
		case '\n':
		case '\t':
			continue;
		default:
			return Error<std::vector<Token>>("Unexpected character.");
		}
	}

	return Ok(Tokens);
}

auto Lexer::LexCharToTokenType(char Char) -> std::optional<TokenType>
{
	switch (Char)
	{
		case ';':
			return TokenType::Semicolon;
		case '(':
			return TokenType::LeftParen;
		case ')':
			return TokenType::RightParen;
		default:
			return std::nullopt;
	}
}
