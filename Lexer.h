
#pragma once

#include <string>
#include <vector>
#include <optional>
#include "Token.h"

class Lexer
{
public:
	auto Lex(const std::string& SourceCode) -> Result<std::vector<Token>>;

private:
	static auto LexCharToTokenType(char Char) -> std::optional<TokenType>;
};
