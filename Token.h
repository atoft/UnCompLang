
#pragma once

#include <string_view>
#include <string>
#include "Globals.h"

enum class TokenType : u8
{
	Semicolon,
	LeftParen,
	RightParen,
	Identifier,
	String,
};

struct Token
{
	// u32 LineNumber = 0;
	// TODO Char number?
	// TODO Source file?

	TokenType Type;
	std::string_view TokenString;

	static Token MakeFromSource(TokenType Type, const std::string::const_iterator& Begin, const std::string::const_iterator& End);
};
