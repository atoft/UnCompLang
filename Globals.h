
#pragma once

#include <cstdint>
#include <optional>
#include <variant>

using u8 = std::uint8_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;

struct CompilationError
{
	std::string Message;
	// TODO Contains source location.
};

template <typename T>
struct Result
{
	std::variant<T, CompilationError> Data;

	bool IsOk() const { return std::holds_alternative<T>(Data); }
	bool IsErr() const { return std::holds_alternative<CompilationError>(Data); }

	std::optional<T> Get() const { if (IsOk()) { return std::get<T>(Data); } else { return std::nullopt; } }
	std::optional<T> Unwrap() const { assert(IsOk()); std::get<T>(Data); }
};

template<typename T>
Result<T> Ok(T Value)
{
	return { Value };
}

template<typename T>
Result<T> Error(const std::string& Message)
{
	return { CompilationError { Message } };
}

template<typename T>
Result<T> Error(const char* Message)
{
	return { CompilationError { std::string(Message) } };
}

