#include "Token.h"

Token Token::MakeFromSource(TokenType Type, const std::string::const_iterator& Begin, const std::string::const_iterator& End)
{
	return { Type, std::string_view(Begin, End)};
}


